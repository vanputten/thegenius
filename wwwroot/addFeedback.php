<html lang="nl">
    <head>
        <?php include 'includes/head.php' ?>
        <title>The Genius</title>
    </head>
  <body>
    <div class="container">
        <?php include 'includes/homebutton.php' ?>
        <?php include 'includes/profileBox.php' ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="header">
                    <h1>The Genius</h1>
                    <img src="assets/img/geniusLogo.png" alt="Logo genius" class="logo">
                    <h2>Voeg feedback toe</h2>
                </div>
            </div>
        </div>
        <div class="infoblock">
            <h3>Je schrijft feedback over: <a href="" class="docTitle"></a></h3>
            <p>Deze feedback zal geplaatst worden in de opmerkingen van het document. Wees zo specifiek mogelijk in je beschrijving, zorg ervoor dat iemand die niet heeft meegewerkt aan dit document het ook kan begrijpen.</p>
        </div>
        <form action="">
            <div class="form-group">
                <label for="feedbackSubject">Onderwerp (optioneel)</label>
                <input class="form-control" type="text" id="feedbackSubject" placeholder="Bijvoorbeeld hoeveelheid uren, contact met klant">
            </div>
            <div class="form-group">
                <label for="feedbackContent">Feedbacktekst*</label>
                <div>
                    <textarea required rows="10" class="form-control" placeholder="Wees specifiek en noteer zowel dingen die goed gingen als dingen die minder goed gingen" id="feedbackContent"></textarea>
                </div>
            </div>
            <div id="submit"><span class="fa fa-save"></span><span>Sla op</span></div>
        </form>
        <div class="preloader">
            <img src="assets/img/preloader.gif" alt="preloader gif loading">
        </div>
        <div class="selectorHolder">
            <div class="buttonHolder"></div>
        </div>
    </div>
    <script src="/assets/js/combined.min.js"></script>
  </body>
</html>