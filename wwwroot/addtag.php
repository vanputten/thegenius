<html lang="nl">
    <head>
        <?php include 'includes/head.php' ?>
        <title>The Genius</title>
    </head>
  <body>
    <div class="container">
        <?php include 'includes/homebutton.php' ?>
        <?php include 'includes/profileBox.php' ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="header">
                    <h1>The Genius</h1>
                    <img src="assets/img/geniusLogo.png" alt="Logo genius" class="logo">
                    <h2>Voeg tag toe</h2>
                </div>
            </div>
        </div>
        <form action="">
            <div class="form-group">
                <label for="typeTag">Type tag*</label>
                <select required id="typeTag" class="form-control">
                    <option value="null" disabled selected>Kies een type</option>
                    <option value="client" disabled>Klant (voor een nieuwe klanttag, maak een nieuwe map aan in "NIEUW GO Klanten"</option>
                    <option value="segment">Segment</option>
                    <option value="custom">Overig</option>
                </select>
            </div>
            <div class="form-group">
                <label for="nameTag">Naam van de tag*</label>
                <input required class="form-control" type="text" placeholder="Bijvoorbeeld klantnaam, keyword research of voorstel" id="nameTag">
            </div>
            <div class="form-group">
                <label for="nameTag">Tag (Wordt automatisch gegenereerd op basis van naam)</label>
                <input disabled required class="form-control" type="text" placeholder="Indien geen tag verschijnt, klik dan op dit veld." id="tagTag">
            </div>
            <!-- <div class="form-group">
                <label for="description">Beschrijving van de tag</label>
                <input type="textarea" class="form-control">
            </div> -->
            <div id="submit"><span class="fa fa-plus"></span><span>Voeg toe</span></div>
        </form>
        <div class="preloader">
            <img src="assets/img/preloader.gif" alt="preloader gif loading">
        </div>
        <div class="selectorHolder">
            <div class="buttonHolder"></div>
        </div>
    </div>
    <script src="/assets/js/combined.min.js"></script>
  </body>
</html>