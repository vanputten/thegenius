<html lang="nl">
    <head>
        <?php include 'includes/head.php' ?>
        <title>The Genius</title>
    </head>
  <body>
    <div class="container">
        <?php include 'includes/homebutton.php' ?>
       <?php include 'includes/profileBox.php' ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="header">
                    <h1>The Genius</h1>
                    <img src="assets/img/geniusLogo.png" alt="Logo genius" class="logo">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div id="googleSignIn"></div>
            </div>
        </div>
        <div class="preloader">
            <img src="assets/img/preloader.gif" alt="preloader gif loading">
        </div>
        <div id="output" class="outputBlock">
            <table>
                <tr>
                    <th>Type</th>
                    <th>Titel</th>
                    <th>Klant</th>
                    <th>Segment</th>
                    <th>Overig</th>
                    <th>Tool</th>
                    <th>Best practice</th>
                </tr>
            </table>
        </div>
    </div>
    <script src="/assets/js/combined.min.js"></script>
  </body>
</html>