<?php
	$fileContents = file_get_contents('tags.json');
	$dataArray = json_decode($fileContents, true);
	$tagArray = array('title' => $_POST['title'], 'tag' => $_POST['tag']);
	if ($_POST['type'] == 'custom'){
		array_push($dataArray["custom"], $tagArray);
	} elseif ($_POST['type'] == 'segment'){
		array_push($dataArray["segments"], $tagArray);
	}
	$jsonData = json_encode($dataArray);
	file_put_contents('tags.json', $jsonData);

	$response_array['status'] = 'success';  
	header('Content-type: application/json');
    echo json_encode($response_array);
?>