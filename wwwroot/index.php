<html lang="nl">
    <head>
        <?php include 'includes/head.php' ?>
        <title>The Genius</title>
    </head>
  <body class="homepage">
    <div class="container">
        <?php include 'includes/homebutton.php' ?>
       <?php include 'includes/profileBox.php' ?>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="header">
                    <h1>The Genius</h1>
                    <img src="assets/img/geniusLogo.png" alt="Logo genius" class="logo">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div id="googleSignIn"></div>
            </div>
        </div>
        <div class="selectorHolder">
            <div class="buttonHolder">
                <a class="button large fixedWidth" href="detailpage#client"><span class="fa fa-male"></span><span>Klant</span></a>
                <a class="button large fixedWidth" href="detailpage#segment"><span class="fa fa-puzzle-piece"></span><span>Segment</span></a>
                <a class="button large fixedWidth" href="detailpage#tool"><span class="fa fa-wrench"></span><span>Tools</span></a>
                <a class="button large fixedWidth" href="detailpage#bestpractice"><span class="fa fa-trophy"></span><span>Best practices</span></a>
                <a class="button large fixedWidth" href="detailpage#custom"><span class="fa fa-hashtag"></span><span>Overig</span></a>
                <a class="button large fixedWidth disabled" href="" onclick="event.preventDefault()"><span class="fa fa-filter"></span><span>Geavanceerd filteren</span></a>
                <a class="button large fixedWidth" href="addtag"><span class="fa fa-plus"></span><span>Voeg tag toe</span></a>
                <input type="text" disabled class="freeSearch disabled" placeholder="Zoek op meerdere, door komma's gescheiden, tags.">
                <a class="button large fixedWidth disabled" href="" onclick="event.preventDefault()"><span class="fa fa-search"></span><span>Zoek</span></a>
            </div>
            <!-- <input type="text" placeholder="Zoek tags..." class="searchQuery"> -->
            <!-- <span class="tags">Tags:</span> -->
            <!-- <input type="submit" class="submitSearch"> -->
        </div>
        <div id="output" class="outputBlock">
            
        </div>
    </div>
    <script src="/assets/js/combined.min.js"></script>
  </body>
</html>