/* EDIT THIS */
var port = 9001;
var javascripts = [
	'vendor/jquery.js',
	'vendor/bootstrap.min.js',
	'platform.js'
];
/* END EDIT THIS */

/* DONT EDIT */
var realJavascripts = [];
javascripts.forEach(function(value, key){
	realJavascripts.push('../build/js/' + value);
});

var root = '../build/sass/main.scss';
var sassFile = '../build/sass/main.scss';

module.exports = function(grunt) {
	require('time-grunt')(grunt);

	var livereload = false;

	if(grunt.option( "reload" )) {
		livereload = {
			port: port
		};
	}

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		watch: {
			all: {
				files: ['../build/**', '../../source/app/Resources/views/**']
			},
			sass: {
				files: ['../build/sass/**'],
				tasks: ['sass:dist', 'replace:sourcemapFix', 'copy:css']
			},
			js: {
				files: '../build/js/**',
				tasks: ['uglify', 'copy:js']
			},
			options: {
				livereload: livereload
			}
		},
		sass: {
			dist: {
				options: {
					sourcemap: true,
					style: "compressed"
				},
				files: {
			        '../../wwwroot/assets/css/dev.combined.min.css': sassFile
		      	}
			}
		},
		uglify: {
			main: {
				options: {
					sourceMap: true,
					sourceMapIncludeSources: true,
					compress: false
				},
				files: {
					'../../wwwroot/assets/js/dev.combined.min.js': realJavascripts,
				},
			},
		},
		replace: {
			sourcemapFix: {
				src: ['../../wwwroot/assets/css/dev.combined.min.css.map'],
				dest: '../../wwwroot/assets/css/',
				replacements: [
					{
						from: '../../../static/build/sass/',
						to: 'assets/css/'
					}
				]
			}
		},
		copy: {
			css: {
				src: '../../wwwroot/assets/css/dev.combined.min.css',
				dest: '../../wwwroot/assets/css/combined.min.css',
				options: {
					process: function (content, srcpath) {
				        return content.replace("/*# sourceMappingURL=dev.combined.min.css.map */", "");
			      }
				}
			},
			js: {
				src: '../../wwwroot/assets/js/dev.combined.min.js',
				dest: '../../wwwroot/assets/js/combined.min.js',
				options: {
					process: function (content, srcpath) {
				        return content.replace("//# sourceMappingURL=dev.combined.min.js.map", "");
			      }
				}
			},
		},
	});


	grunt.registerTask('default', ['watch']);
	grunt.registerTask('build', ['sass', 'replace:sourcemapFix', 'uglify', 'copy']);
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-text-replace');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify');
};
/* END DONT EDIT */