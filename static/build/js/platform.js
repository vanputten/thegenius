$(function() {
    var browserKey = 'AIzaSyCGX_wfti87nACR6_mtXGBlDVQyZVUWoa4';
    var platform = {
        init: function(){
            google.init();
            platform.addEventListeners();
            if (localStorage){
                var user = {
                    email: localStorage.email,
                    name: localStorage.name,
                    picture: localStorage.picture
                }
            }
        },
        addEventListeners: function(){
            $('.signOut').on('click', function(e){
                e.preventDefault();
                google.signOut();
            })
            $('.submitSearch').on('click', function(e){
                $('#output table').find("tr:gt(0)").remove();
                console.log('submit');
                e.preventDefault();
                google.searchDriveByTag($('select.tags').val());
            })

            $('#output').on('click', '.updateTags', function(e){
                e.preventDefault();
                $(this).val('Bezig...');
                tagger.updateTags($(this));
            })
            $('#output').on('click', '.updateAllTags', function(e){
                e.preventDefault();
                $(this).val('Bezig...');
                tagger.updateAllTags($(this));
            })
            if(window.location.href.indexOf("tagfromdrive") > -1) {
               tagger.init(window.location);
            }

            if(window.location.href.indexOf("addtag") > -1) {
                var email = localStorage.email;
                var lastIndex = email.lastIndexOf("@")

                var s1 = email.substring(0, lastIndex); //after this s1="Text1, Text2, Text"
                var domain = email.substring(lastIndex + 1); //after this s2="true"
                if(localStorage.email && domain == "greenorange.com"){
                   $('form').on('blur', 'input#nameTag', function(){
                        var tag;
                        var name = $(this).val();
                        var words = name.split(' ');
                        if (name.length < 6){
                            tag = name;
                        } else if(words.length == 1){
                            tag = name.substring(0, 6);
                        } else {
                            var tag = null;
                            $.each(words, function(index, val){
                                if (tag == null){
                                    tag = val.substring(0, 3);
                                } else {
                                    tag += val.substring(0, 3);
                                }
                            })
                        }
                        $('input#tagTag').val(tag.toLowerCase());
                   })

                   $('form').on('click', '#submit', function(e){
                        var type = $('form #typeTag').val();
                        var name = $('form #nameTag').val();
                        var tag = $('form #tagTag').val();
                        tagger.addTag(type, name, tag, function(status){

                        });
                   })
                } else {
                    window.location.replace('/');
                    alert('Het is de huidige gebruiker niet toegestaan om nieuwe tags aan te maken, log in met een @greenorange.com account om tags toe te voegen.');
                }
            }

            if(window.location.href.indexOf("addFeedback") > -1) {
                hash = window.location.hash.substring(1);
                google.checkAuth(function(result){
                    google.getFile(hash, function(file){
                        $('.docTitle').text(file.name).attr('href', file.webViewLink).attr('data-id', hash);
                    });
                });

                $('form').on('click', '#submit', function(e){
                    $('#submit').html('<span class="fa fa-gear fa-spin"></span><span>Bezig...</span>')
                    var subject = $('form #feedbackSubject').val();
                    var content = $('form #feedbackContent').val();
                    var id = $('.docTitle').data('id');
                    feedbacker.send(id, content, subject);
                })
            }

            if(window.location.href.indexOf("detailpage") > -1) {
               $('.selectorHolder .buttonHolder').on('click', 'a', function(e){
                    $('.selectorHolder').hide();
                    $('.preloader').show();
                    if ($(this).attr("href") !== 'addtag'){
                        e.preventDefault();
                        hash = window.location.hash.substring(1);
                        // if (hash !== 'filter'){
                            console.log($(this).data('tag'));
                            console.log(hash);
                            searcher.getFiles($(this).data('tag'), hash);
                            history.pushState({}, '', '#results');
                        // }
                    }
               })

                onpopstate = function(event){
                    $('#output').hide();
                    $('.preloader').fadeIn(100);
                    searcher.init(window.location);
                }
                hash = window.location.hash.substring(1);
                if (hash == 'client' || hash == 'segment' || hash == 'custom'){
                    console.log('searcher init');
                    searcher.init(window.location);
                } else {
                    $('.preloader').show();
                    hash = window.location.hash.substring(1);
                    searcher.getFiles($(this).data('tag'), hash);
                    history.pushState({}, '', '#results');
                }
            }

            $('#output').on('click', '.feedbackButton', function(e){
                var id = $(this).closest('tr').data('id');
                window.location.href = 'addFeedback#' + id;
            });

            $('#output').on('click', '.editButton', function(e){
                var url = 'tagfromdrive?state={"ids":['
                $.each($('#output table tr'), function(index, val) {
                    if ($(this).data('id')){
                        url += '"' + $(this).data('id') + '",';
                    }
                });
                console.log(url);
                url = url.slice(0, url.length-1);
                console.log(url);
                url += ']}';
                window.location.href = url;
            });
            
        }
    }
    var google = {
        init: function(){
            gapi.signin2.render('googleSignIn', {
                'scope': 'email https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.install',
                'width': 400,
                'height': 100,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': google.onSignIn,
                'onfailure': google.failSignIn
            })
        },
        isSignedIn: function(){
            var curUser = gapi.auth2.getAuthInstance().currentUser.get();
            if(curUser.isSignedIn()){
                return true
                $('.topRight').show();
            } else {
                return false
                $('.topRight').hide();
            }
        },
        onSignIn: function(googleUser){
             // Useful data for your client-side scripts:
            var profile = googleUser.getBasicProfile();
            $('.topRight .name').text(profile.getEmail());
            $('.topRight .email').text(profile.getName());
            $('.topRight .profilepic').attr('src', profile.getImageUrl());
            $('#googleSignIn').hide();
            $('.selectorHolder').show();
            $('.topRight').show();
            google.checkAuth(function(authResult){
                // The ID token you need to pass to your backend:
                var id_token = googleUser.getAuthResponse().id_token;
                $.post( "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + id_token, function( data ) {
                    $( ".getFolders" ).html( data );
                    localStorage.email = data.email;
                    localStorage.name = data.name;
                    localStorage.picture = data.picture;
                    localStorage.sub = data.sub;
                    if ($('body').hasClass('homepage')){
                    tagger.listTags(function(tags){
                        var clients = tags.clients;
                        var segments = tags.segments;
                        var custom = tags.custom;
                        var tagsSelect = '<select class="tags"><option disabled selected>Kies een tag</option>';
                        var tagList;
                        $.each(clients, function(index, val){
                            tagList += '<option value="' + val.tag + '">' + val.title + '</option>'
                        })
                        $.each(segments, function(index, val){
                            tagList += '<option value="' + val.tag + '">' + val.title + '</option>'
                        })
                        $.each(custom, function(index, val){
                            tagList += '<option value="' + val.tag + '">' + val.title + '</option>'
                        })
                        
                        tagsSelect += tagList;
                        tagsSelect += '</select>';
                        $('.tags').append(
                            tagsSelect
                        );
                    });
                    }
                });
            });
        },
        signOut: function(){
            if (google.isSignedIn()){
                var curAuth = gapi.auth2.getAuthInstance();
                curAuth.disconnect()
                curAuth.signOut();
                window.location = "/"
            }
        },
        checkAuth: function(callback){
            setTimeout(function() {
                gapi.auth.authorize({
                    'client_id': '326325479658-5jsq00qioqsckpn2vfbm69ejq8k6v9at',
                    'scope': 'https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/drive.install',
                    'immediate': true
                }, function(authResult){
                    if (authResult.status.signed_in) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                });
            }, 500);
        },
        searchDrive: function(query){
            gapi.client.load('drive', 'v3', function(){
                var request = gapi.client.drive.files.list({
                    'q': "name contains '" + query + "'"
                });

                request.execute(function(resp) {
                    var files = resp.files;
                    if (files && files.length > 0) {
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            $('#output').append('<a href="#" data-id="' + file.id + '">' + file.name + '</a><br>');
                        }
                    } else {
                        $('#output').append('No files found');
                    }
                    $('#output').fadeIn(200);
                });
            });
        },
        searchDriveByTag: function(tag){
            gapi.client.load('drive', 'v3', function(){
                var request = gapi.client.drive.files.list({
                    'q': 'appProperties has {key="client" and value="' +  tag +'"} or appProperties has {key="segment" and value="' + tag +'"} or appProperties has {key="custom" and value="' + tag +'"}'
                });

                request.execute(function(resp) {
                    var files = resp.files;
                    if (files && files.length > 0) {
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            $('#output').append('<a href="#" data-id="' + file.id + '">' + file.name + '</a><br>');
                        }
                    } else {
                        $('#output').append('No files found');
                    }
                    $('#output').fadeIn(200);
                });
            });
        },
        getFile: function(fileId, callback){
            var request = gapi.client.request({
               path : 'https://www.googleapis.com/drive/v3/files/' + fileId + '?fields=iconLink,id,kind,mimeType,appProperties,name,webViewLink,modifiedTime&key=' + browserKey,
               method : 'GET'
            });
            request.execute(function(response) {
                console.log(response);
                if (response.error){
                    callback(response, fileId);
                } else {
                    callback(response);
                }
            });
        }
    }
    var tagger = {
        init: function(url){
            $('.preloader').show();
            google.checkAuth(function(result){
                if (result){
                    var decoded = decodeURI(url.search.slice(7));
                    var parsed = jQuery.parseJSON(decoded);
                    var data;
                    if (parsed.ids && parsed.exportIds){
                        data = parsed.ids.concat(parsed.exportIds);
                    } else if (parsed.ids) {
                        data = parsed.ids;
                    } else if (parsed.exportIds) {
                        data = parsed.exportIds;
                    }
                    var fileList = [];
                    var prevIndex;
                    var retry = 0;
                    var index = 0;
                    var dataLength = data.length;
                    loopFiles();
                    function loopFiles(){
                        if (prevIndex == index){
                            retry++;
                        } else {
                            retry = 0;
                        }
                        prevIndex = index;
                        setTimeout(function() {
                            google.getFile(data[index], function(results){
                                if (results.error){
                                    loopFiles();
                                } else {
                                    index++;
                                    fileList.push(results);
                                    console.log(index);
                                    console.log(dataLength);
                                    if (index == dataLength || fileList.length == 5){
                                        $('#output').fadeIn(200);
                                        if (index == dataLength){
                                            tagger.addFilesToList(fileList, true);
                                        } else {
                                            tagger.addFilesToList(fileList);  
                                        }
                                        fileList = [];
                                    }
                                    if (index != dataLength){
                                        loopFiles();
                                    }
                                }
                            })
                        }, 50*retry);
                    }
                }
            })
        },
        addFilesToList: function(fileList, last){
            if (! last){
                last = false;
            }
            tagger.listTags(function(tags){
                var clients = tags.clients;
                console.log(tags);
                console.log(clients);
                var segments = tags.segments;
                var custom = tags.custom;
                var clientsSelect = '<dl class="dropdown clients"><dt><a href="#"><span class="hida">Toon tag(s)</span><p class="multiSel"></p></a></dt><dd><div class="mutliSelect"><ul>';
                // var clientsSelect = '<select class="clients"><option disabled selected>Kies een tag</option>';
                var segmentsSelect = '<dl class="dropdown segments"><dt><a href="#"><span class="hida">Toon tag(s)</span><p class="multiSel"></p></a></dt><dd><div class="mutliSelect"><ul>';
                var customSelect = '<dl class="dropdown custom"><dt><a href="#"><span class="hida">Toon tag(s)</span><p class="multiSel"></p></a></dt><dd><div class="mutliSelect"><ul>';
                $.each(clients, function(index, val){
                    clientsSelect += '<li><input type="checkbox" value="' + val.tag + '"/>' + val.title + '</li>'
                })
                $.each(segments, function(index, val){
                    segmentsSelect += '<li><input type="checkbox" value="' + val.tag + '"/>' + val.title + '</li>'
                })
                $.each(custom, function(index, val){
                    customSelect += '<li><input type="checkbox" value="' + val.tag + '"/>' + val.title + '</li>'
                })
                clientsSelect += '</ul></div></dd></dl>';
                segmentsSelect += '</ul></div></dd></dl>';
                customSelect += '</ul></div></dd></dl>';
                $.each(fileList, function(index, val){
                    $('#output table > tbody:last-child').append(
                        '<tr data-id="' + val.id + '"><td>' +
                        '<img src="' + val.iconLink + '"></td><td>' +
                        '<a href="' + val.webViewLink +'" target="_blank">' + val.name + '</a></td><td>' +
                        clientsSelect + '</td><td>' +
                        segmentsSelect + '</td><td>' +
                        customSelect + '</td><td>' +
                        '<input type="checkbox" class="tool"></td><td>' +
                        '<input type="checkbox" class="bestpractice"></td><td>' +
                        '<input type="submit" class="updateTags">' +
                        '</tr>'
                    );
                    var tool;
                    var bestPractice;
                    $.each(val.appProperties, function(index, val){
                        if (index == 'client' || 'segment' || 'custom'){
                            var array = val.split(',')
                            $.each(array, function(index, val){
                                $('#output table > tbody tr:last').find("input[value='"+val+"']").prop('checked', true);
                            });
                        }
                        if (index == 'tool' || 'bestpractice'){
                            if (val == 'true'){
                                $('#output table > tbody tr:last').find('input[class="'+index+'"]').prop('checked', true);
                            } else {
                                $('#output table > tbody tr:last').find('input[class="'+index+'"]').prop('checked', false);
                            }
                        }
                    });
                });
                if (last){
                    $('#output table > tbody:last-child').append(
                        '<tr class="tagAllAs"><td colspan="2"><strong>Tag alles als</strong></td><td>' +
                        clientsSelect + '</td><td>' +
                        segmentsSelect + '</td><td>' +
                        customSelect + '</td><td>' +
                        '<input type="checkbox" class="tool"></td><td>' +
                        '<input type="checkbox" class="bestpractice"></td><td>' +
                        '<input type="submit" class="updateAllTags">' +
                        '</tr>'
                    );
                    $('.preloader').hide();
                    $(".dropdown dt a").on('click', function() {
                        $(this).parents('.dropdown').find('dd ul').slideToggle('fast');
                      // $(".dropdown dd ul").slideToggle('fast');
                    });

                    $(".dropdown dd ul li a").on('click', function() {
                        $(this).closest('ul').hide();
                      // $(".dropdown dd ul").hide();
                    });

                    function getSelectedValue(id) {
                      return $("#" + id).find("dt a span.value").html();
                    }

                    $(document).bind('click', function(e) {
                      var $clicked = $(e.target);
                      if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function() {
                        
                    });
                }
                
            });
        },
        listTags: function(callback){
            gapi.load('client', function(){
                gapi.client.load('drive', 'v3', function(){
                    google.checkAuth(function(result){
                        if (result){
                            var request = gapi.client.drive.files.list({
                                'q': '"0B99sTiUW4ICTWV9SMUhmUE1QRTg" in parents'
                            });
                            var listOfClients = [];
                            request.execute(function(resp) {
                                console.log(resp);
                                $.each(resp.files, function(index, val) {
                                    if (val.mimeType == 'application/vnd.google-apps.folder'){
                                        var words = val.name.split(' '); 
                                        if (val.name.length < 6){
                                            tag = val.name;
                                        } else if(words.length == 1){
                                            tag = val.name.substring(0, 6);
                                        } else {
                                            var tag = null;
                                            $.each(words, function(index, val){
                                                if (tag == null){
                                                    tag = val.substring(0, 3);
                                                } else {
                                                    tag += val.substring(0, 3);
                                                }
                                            })
                                        }
                                        listOfClients.push({
                                            'title': val.name,
                                            'tag': tag.toLowerCase()
                                        });
                                    }
                                });
                                function SortByName(a, b){
                                  var aName = a.title.toLowerCase();
                                  var bName = b.title.toLowerCase(); 
                                  return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                                }

                                listOfClients.sort(SortByName);
                                var tags = [];
                                tags['clients'] = listOfClients;
                                console.log(tags);
                                $.ajaxSettings.cache = false;
                                $.getJSON( "json/tags.json", function(data) {
                                    tags['segments'] = data.segments;
                                    tags['custom'] = data.custom;
                                    callback(tags);
                                });
                            });
                        }
                    });
                });
            })
        },
        updateTags: function(item){
            var thisRow = item.closest('tr');
            var clients = $(thisRow).find('.dropdown.clients').find('.mutliSelect input');
            var segments = $(thisRow).find('.dropdown.segments').find('.mutliSelect input');
            var customs = $(thisRow).find('.dropdown.custom').find('.mutliSelect input');
            var client = [];
            var segment = [];
            var custom = [];
            var breakFunction = false;
            tagger.removeAllTags(thisRow, function(){
                $.each(clients, function(index, val) {
                    if (val.checked){
                        client.push($(val).val());
                        if (client.length > 3){
                            alert('Er zijn teveel tags van het type klant, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 3.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });

                if (client.length > 0 && breakFunction == false){
                    for (var i = 0; i < client.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'client' + i, client[i]);                  
                    };
                };

                $.each(segments, function(index, val) {
                    if (val.checked){
                        segment.push($(val).val());
                        if (segment.length > 7){
                            alert('Er zijn teveel tags van het type segment, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 6.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });
                if (segment.length > 0 && breakFunction == false){
                    for (var i = 0; i < segment.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'segment' + i, segment[i]);                    
                    };
                };

                $.each(customs, function(index, val) {
                    if (val.checked){
                        custom.push($(val).val());
                        if (custom.length > 7){
                            alert('Er zijn teveel tags van het type overig, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 6.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });
                if (custom.length > 0 && breakFunction == false){
                    for (var i = 0; i < custom.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'custom' + i, custom[i]); 
                    };
                };
                var tool = $(thisRow).find('input.tool').prop("checked");
                tagger.patchTag($(thisRow).data('id'), 'tool', tool);
                var bestpractice = $(thisRow).find('input.bestpractice').prop("checked");
                tagger.patchTag($(thisRow).data('id'), 'bestpractice', bestpractice);
                console.log(tool);
                console.log(bestpractice);
                // Remove all the old, non-indexed tags from a document
                // tagger.patchTag($(thisRow).data('id'), 'client', null);
                // tagger.patchTag($(thisRow).data('id'), 'segment', null);
                // tagger.patchTag($(thisRow).data('id'), 'custom', null);
                setTimeout(function() {
                    $(item).val('Verzenden');
                }, 2500);
            });
        },
        updateAllTags: function(item){
            var listIds = [];
            $.each($('tr[data-id]'), function(index, val) {
                listIds.push($(this).data('id'));
                console.log(listIds);
                var tagAllRow = $('.tagAllAs')
                var thisRow = $(this);
                var clients = $(tagAllRow).find('.dropdown.clients').find('.mutliSelect input');
                var segments = $(tagAllRow).find('.dropdown.segments').find('.mutliSelect input');
                var customs = $(tagAllRow).find('.dropdown.custom').find('.mutliSelect input');
                var client = [];
                var segment = [];
                var custom = [];
                var breakFunction = false;
                $.each(clients, function(index, val) {
                    if (val.checked){
                        client.push($(val).val());
                        if (client.length > 3){
                            alert('Er zijn teveel tags van het type klant, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 3.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });

                if (client.length > 0 && breakFunction == false){
                    for (var i = 0; i < client.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'client' + i, client[i]);                  
                    };
                };

                $.each(segments, function(index, val) {
                    if (val.checked){
                        segment.push($(val).val());
                        if (segment.length > 7){
                            alert('Er zijn teveel tags van het type segment, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 6.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });
                if (segment.length > 0 && breakFunction == false){
                    for (var i = 0; i < segment.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'segment' + i, segment[i]);                    
                    };
                };

                $.each(customs, function(index, val) {
                    if (val.checked){
                        custom.push($(val).val());
                        if (custom.length > 7){
                            alert('Er zijn teveel tags van het type overig, verwijder één of meerdere tags om plaats te maken voor nieuwe. Het maximum aantal voor dit type is 6.');
                            $(item).val('Verzenden');
                            return false;
                            breakFunction = true;
                        }
                    }
                });
                if (custom.length > 0 && breakFunction == false){
                    for (var i = 0; i < custom.length; i++) {
                        tagger.patchTag($(thisRow).data('id'), 'custom' + i, custom[i]); 
                    };
                };
                var tool = $(tagAllRow).find('input.tool').prop("checked");
                tagger.patchTag($(thisRow).data('id'), 'tool', tool);
                var bestpractice = $(tagAllRow).find('input.bestpractice').prop("checked");
                tagger.patchTag($(thisRow).data('id'), 'bestpractice', bestpractice);
                // Remove all the old, non-indexed tags from a document
                // tagger.patchTag($(thisRow).data('id'), 'client', null);
                // tagger.patchTag($(thisRow).data('id'), 'segment', null);
                // tagger.patchTag($(thisRow).data('id'), 'custom', null);
                setTimeout(function() {
                    $(item).val('Verzenden');
                }, 2500);
            });
        },
        patchTag: function(fileId, key, value, cb){
            gapi.load('client', function(){
                gapi.client.load('drive', 'v3', function(){
                    var tags = {};
                    tags[key] = value;
                    console.log(tags);
                    console.log(fileId);
                    var request = gapi.client.drive.files.update({
                        'fileId': fileId,
                        'appProperties': tags
                    });
                    request.execute(function(resp) {
                        if (cb){
                            cb();
                        }
                    });
                });
            });
        },
        removeAllTags: function(row, callback){
            google.getFile($(row).data('id'), function(file){
                var curTags = [];
                if (file.appProperties){
                    curTags = file.appProperties;
                    for (var i = 0; i < curTags.length; i++) {
                        tagger.patchTag($(row).data('id'), curTags[i].key, null);
                    };
                }

                callback();
            })
        },
        addTag: function(type, name, tag, callback){
            if (type && name && tag){
                var tags = [];
                var exists = false;
                $.ajaxSettings.cache = false;
                $.getJSON( "json/tags.json", function(data) {
                    tags.push(data.segments);
                    tags.push(data.custom);
                    console.log(tags);

                    $.each(tags, function(index, val) {
                        $.each(val, function(index, val) {
                            if (val['tag'] == tag || val['title'] == name){
                                exists = true;
                            }
                        });
                    });
                    if (exists){
                        alert('Er bestaat al een tag met deze naam of tag! Controleer goed of het aanmaken van deze tag noodzakelijk is!');
                    } else {
                        console.log('hallo');
                        $.ajax({
                            type: "POST",
                            dataType : 'json',
                            url: 'json/save_tag.php',
                            data: {title: name, tag: tag, type: type},
                            success: function (response) {
                                alert("De nieuwe tag is aangemaakt");
                                $('form').trigger("reset");
                            },
                            failure: function(response) {
                                alert("Error! " + response);
                            }
                        });
                    }
                });
            } else {
                alert('Zorg dat alle velden zijn ingevuld!');
            }
        }
    }
    var searcher = {
        init: function(url){
            $('.preloader').show();
            type = url.hash.substring(1);
            tagger.listTags(function(result){
                if (type == "client"){
                    $.each(result.clients, function(index, val) {
                        $('.buttonHolder').append('<a class="button" href="#' + val.tag + '" data-tag="' + val.tag + '"><span>' + val.title + '</span></a>')
                    });
                } else if (type == 'segment'){
                    $.each(result.segments, function(index, val) {
                        $('.buttonHolder').append('<a class="button" href="#' + val.tag + '" data-tag="' + val.tag + '"><span>' + val.title + '</span></a>')
                    });
                } else if (type == 'custom'){
                    $.each(result.custom, function(index, val) {
                        $('.buttonHolder').append('<a class="button" href="#' + val.tag + '" data-tag="' + val.tag + '"><span>' + val.title + '</span></a>')
                    });
                }
                $('.buttonHolder').append('<a class="button" href="addtag"><span class="fa fa-plus"></span><span>Voeg tag toe</span></a>');
                $('.preloader').hide();
                $('.selectorHolder').fadeIn(200);
            })
        },
        getClientFiles: function(clientTag){
            gapi.client.load('drive', 'v3', function(){
                google.checkAuth(function(result){
                    if (result){
                        var request = gapi.client.drive.files.list({
                            'q': 'appProperties has {key="client" and value="' +  clientTag +'"}'
                        });

                        request.execute(function(resp) {
                            var files = resp.files;
                            if (files && files.length > 0) {
                                var fileList = [];
                                $.each(files, function(index, val) {
                                    var dataLength = files.length;
                                    google.getFile(val.id, function(results){
                                        fileList.push(results);
                                        if (fileList.length == dataLength){
                                            searcher.addFilesToList(fileList);
                                        }
                                    })
                                });
                            } else {
                                $('#output').append('No files found');
                                $('#output').fadeIn(100);
                            }
                        });
                    }
                });
            });
        },
        getFiles: function(query, type){
            var thisType = type;
            $('.preloader').show();
            if (!query){
                query = true;
            }
            setTimeout(function() {
                gapi.client.load('drive', 'v3', function(){
                    google.checkAuth(function(result){
                        if (result){
                            var q = '';
                            if (thisType == 'client' || thisType == 'segment' || thisType == 'custom'){
                                for (var i = 0; i < 7; i++) {
                                    q += 'appProperties has {key="' + thisType + i + '" and value="' + query + '"}';
                                    if (i < 6){
                                        q += ' or '
                                    }
                                };
                                console.log(q);
                                var request = gapi.client.drive.files.list({
                                    'q': q
                                });
                            } else {
                                var request = gapi.client.drive.files.list({
                                    'q': 'appProperties has {key="' + type +'" and value="' +  query +'"}'
                                })
                            }
                            tagger.listTags(function(tags){
                                request.execute(function(resp) {
                                    var files = resp.files;
                                    if (files && files.length > 0) {
                                        var fileList = [];
                                        var dataLength = files.length;
                                        var counter = 0;
                                        var delay = 1;
                                        var prevIndex;
                                        var retry = 0;
                                        var index = 0;
                                        loopFiles();
                                        function loopFiles(){
                                            if (prevIndex == index){
                                                retry++;
                                            } else {
                                                retry = 0;
                                            }
                                            prevIndex = index;
                                            setTimeout(function() {
                                                google.getFile(files[index].id, function(results, fileId){
                                                    if (results.error){
                                                        loopFiles();
                                                    } else {
                                                        index++;
                                                        fileList.push(results);
                                                        if (index == dataLength || fileList.length == 5){
                                                            searcher.addFilesToList(fileList, tags);
                                                            fileList = [];
                                                            if (index == dataLength){
                                                                $('.preloader').hide();
                                                            }
                                                        }
                                                        if (index != dataLength){
                                                            loopFiles();
                                                        }
                                                    }
                                                });
                                            }, 50*retry);
                                        }
                                    } else {
                                        $('#output').append('No files found');
                                        $('.preloader').hide();
                                        $('#output').fadeIn(100);
                                    }
                                });
                            })

                        }
                    });
                });
            }, 500);
        },
        addFilesToList: function(fileList, tagList){
            var tags = tagList
            $.each(fileList, function(index, val){
                var clientsSelect = [];
                var segmentsSelect = [];
                var customSelect = [];
                var tool = '<span class="fa fa-times"></span>';
                var bestPractice = '<span class="fa fa-times"></span>';
                $.each(val.appProperties, function(index, val){
                    if (index.replace(/\d+/g, '') == 'client' && tags.clients){
                        var result = $.grep(tags.clients, function(e){
                            return e.tag == val
                        })
                        clientsSelect.push(result[0].title);

                    }
                    if (index.replace(/\d+/g, '') == 'segment' && tags.segments){
                        var result = $.grep(tags.segments, function(e){
                            return e.tag == val
                        })
                        segmentsSelect.push(result[0].title);
                    }
                    if (index.replace(/\d+/g, '') == 'custom' && tags.custom){
                        var result = $.grep(tags.custom, function(e){
                            return e.tag == val
                        })
                        customSelect.push(result[0].title);
                    }
                    if (index == 'tool' && val == 'true'){
                        tool = '<span class="fa fa-check"></span>'
                    }
                    if (index == 'bestpractice' && val == 'true'){
                        bestPractice = '<span class="fa fa-check"></span>'
                    }
                })
                $('#output table > tbody:last-child').append(
                    '<tr data-id="' + val.id + '"><td>' +
                    '<img src="' + val.iconLink + '"></td><td>' +
                    '<a href="' + val.webViewLink +'" target="_blank">' + val.name + '</a></td><td>' +
                    clientsSelect + '</td><td>' +
                    segmentsSelect + '</td><td>' +
                    customSelect + '</td><td>' +
                    tool + '</td><td>' +
                    bestPractice + '</td><td>' +
                    val.modifiedTime.substring(0, 10) + '</td><td>' +
                    '<span class="editButton fa fa-pencil"></span></td><td>' +
                    '<span class="feedbackButton fa fa-book"></span></td>"' +
                    '</tr>'
                );
            });
            if ($('#output').is(':hidden')){
                $('#output').fadeIn(200);
            }
        }
    }
    var feedbacker = {
        send: function(fileId, commentContent, commentSubject){
            if (fileId && commentContent){
                if (! commentSubject){
                    commentSubject = '';
                }
                var request = gapi.client.request({
                    path : 'https://www.googleapis.com/drive/v3/files/' + fileId + '/comments?fields=anchor%2Cauthor%2Ccontent%2CcreatedTime%2Cdeleted%2ChtmlContent%2Cid%2Ckind%2CmodifiedTime%2CquotedFileContent%2Creplies%2Cresolved',
                    method: 'POST',
                    body: {
                        content: commentContent,
                        quotedFileContent: {
                            value: commentSubject
                        }
                    }
                });
                request.execute(function(response) {
                    $('form #feedbackSubject').val('');
                    $('form #feedbackContent').val('');
                    $('#submit').html('<span class="fa fa-check"></span><span>Opgeslagen!</span>')
                    setTimeout(function() { 
                        $('#submit').html('<span class="fa fa-save"></span><span>Sla op</span>')
                    }, 2500);
                });
            } else {
                alert('Er is geen content ingevoerd of er ging iets anders fout, probeer het opnieuw.')
            }
        }
    }
    platform.init();
});


